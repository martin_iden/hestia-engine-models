## NO3, to groundwater, crop residue decomposition

Nitrate leaching to groundwater, from crop residue decomposition.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [no3ToGroundwaterCropResidueDecomposition](https://hestia.earth/term/no3ToGroundwaterCropResidueDecomposition)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [value](https://hestia.earth/schema/Emission#value)
  - [sd](https://hestia.earth/schema/Emission#sd)
  - [min](https://hestia.earth/schema/Emission#min)
  - [max](https://hestia.earth/schema/Emission#max)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` and a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `True`
  - Data completeness assessment for water: [completeness.water](https://hestia.earth/schema/Completeness#water) must be `True`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) and a list of [properties](https://hestia.earth/schema/Product#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) and a list of [properties](https://hestia.earth/schema/Product#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [belowGroundCropResidue](https://hestia.earth/term/belowGroundCropResidue) and a list of [properties](https://hestia.earth/schema/Product#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
  - optional:
    - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
      - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [waterRegime](https://hestia.earth/glossary?termType=waterRegime)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('no3ToGroundwaterCropResidueDecomposition', Cycle))
```
