# AWARE

This model characterises water use based on the geospatial AWARE model, see [Boulay et al (2018)](https://www.doi.org/10.1007/s11367-017-1333-8).
