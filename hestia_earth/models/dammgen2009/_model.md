# Dämmgen (2009)

These models calculate direct and indirect greenhouse gas emissions from the German GHG inventory guidelines, [Dämmgen (2009)](https://d-nb.info/1009968750/34).
