## Cation exchange capacity (per kg soil)

The total capacity of a soil to hold exchangeable cations. It influences the soil's ability to hold onto essential nutrients and provides a buffer against soil acidification. Measured in centimoles of charge per kg of soil at neutrality (cmolc/kg) which is equivalent to meq/100g.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [cationExchangeCapacityPerKgSoil](https://hestia.earth/term/cationExchangeCapacityPerKgSoil)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [depthUpper](https://hestia.earth/schema/Measurement#depthUpper)
  - [depthLower](https://hestia.earth/schema/Measurement#depthLower)
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `modelled using other physical measurements`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
    - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [clayContent](https://hestia.earth/term/clayContent)
    - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [soilPh](https://hestia.earth/term/soilPh)
    - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [organicCarbonPerKgSoil](https://hestia.earth/term/organicCarbonPerKgSoil)

### Lookup used

- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run('cationExchangeCapacityPerKgSoil', Site))
```
