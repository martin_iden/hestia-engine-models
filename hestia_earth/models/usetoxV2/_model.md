# USEtox v2

This model characterises chemicals' toxicity according to the [USEtox model, version 2.1](https://usetox.org/model/download/usetox2.12). For freshwater ecotoxicity, HC50-EC50 values are used. The HC50-EC50 value represents the hazardous concentration of a chemical at which 50% of the species considered are exposed to a concentration above their EC50.
